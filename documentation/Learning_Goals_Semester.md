## Knowledge
<br>

* The generel working principle of a robot arm.
* The generel working principle of a PLC. (Programmable logic controller)
* Industrial communication protocols.

## Ability
* Implement appropriate industrial interfaces.
* Modify existing products.
* Build solutions which interfaces with industrial equipment.

## Skills
* Gather technical documentation on existing products.
* Design modifications/extensions of existing products.
* Provide technical documentation on modifications/extensions of existing products.

# The generel working principle of a robot arm.
Common types of industrial robots:
* Jointed arm (Universal Robot) - see Cobots.txt file for more info
* SCARA robot (ABB) - see Scara.txt file for more info
* Cartesian robot (Farmbot) - see Cartesian.txt file for more info
* Delta robot (Kawasaki) - see Delta.txt file for more info
source: https://www.evsint.com/industrial-robots-types/
(see image "Common_industrial_robots.png")

Common Grippers:
* Finger gripper
* Vaccum gripper
* Soft gripper
* Magnetic gripper

(see image "grippers.png")

# The generel working principle of a PLC.
Source for more info: http://www.ieec.uned.es/investigacion/Dipseil/PAC/archivos/introtoplcs_SUPER.pdf
<br>

It stands for Programmable logic controller
before PLC's the industry used hardwired panels which were very time consuming to wire, debug and change.  

Advantages of PLC
* Flexible
* Faster response time
* Less and simpler wiring
* Solid-state - no moving parts
* Modular design - easy to repair and expand
* Handles much more complicated systems
* Sophisticated instruction sets available
* Allows for diagnostics “easy to troubleshoot”
* Less expensive

A PLC consists of:
* a processor unit (CPU) which interprets inputs, executes the control program stored in memory and sends output signals,
* a power supply unit which converts AC voltage to DC,
* a memory unit storing data from inputs and program to be executed by the processor,
* an input and output interface, where the controller receives and sends data from/to external devices,
* a communications interface to receive and transmit data on communication networks from/to remote PLCs.

## Simons Notes:
PLC = Programmable logic controller
<br>
Its used in industrial applications for automatation
<br>
A PLC can receive information from a connected sensor or a other device
Once the data is received it can process the data and trigger pre-programmed instructions depending on the output
<br>
PLC's are flexible and robust
<br>
The PLC can take both analog and digital inputs
<br>
PLC's can also be connected to other systems to record data etc.
PLC's can handle a wide range of communication protocols
<br>
To interface with a PLC in real time an HMI is needed
HMI meaning a human machine interface these can vary greatly.
<br>
PLC's is an important part of the IoT world. They can connect to SQL databases to store data, or connect to the cloud in other ways with MQTT etc.
<br>
A PLC program is usually written on a computer and then is downloaded to the controller
<br>
Most PLC programming software offers programming in Ladder Logic, or “C”. Ladder Logic is the traditional programming language. It mimics circuit diagrams with “rungs” of logic read left to right. Each rung represents a specific action controlled by the PLC, starting with an input or series of inputs (contacts) that result in an output (coil). Because of its visual nature, Ladder Logic can be easier to implement than many other programming languages.
“C” programming is a more recent innovation.
Some PLC manufacturers supply control programming software.
<br>
A PLC has to be made very robust, it should be able to survive in harsh enviorments for like 10 years.
<br>
A PLC is often modular, it usually comes with a processor and a power supply and then you just add on so called "cards" that adds diffrent functionality like analog / digital - in/out.
This means that the factory can add only the required functionality and if anything breaks, it can be replaced without having to rebuild the whole system.
<br>
A PLC has optical isolation, because most industrial devices runs on 12v or 25v, which is way too much for the microchip inside of the PLC.
Optical isolation means that the data is transfered using lights and infared sensors.
<br>
A PLC has to have a hard real time clocks, meaning it will be able to run hard RTS. 
(RTS = real time systems)
when running hard RTS like a PLC it will simple just cut off processes that would take too much power if ever stressed. 
Downside of running RTS is you can only run smaller programs.
<br>
PLC program scan cycle = PLC programs runs on cycles and you have to fit your program into a cycle otherwise it'll be cut off. alternativly if its a bigger operation you will have to save to program and load it in the next cycle.
<br>
A cycle typically looks like this:

-->start --> input scan --> program scan -->output scan --> clean up --> 
<br>
PLC programming intro:

Ladder programming language:
made for running in hard RTM systems
made by electrecians, uses basic logic
coils (represented by a circle with holes in the middel) = output
Two straight bars symbol = input (called contacts)
You can also get inverted switches

<br>

# Industrial communication protocols
