import tkinter as tk
from tkinter import *
from tkinter import ttk

main_window = tk.Tk()
main_window.title('UR Movement simulator GUI')
main_window.geometry("640x480")

def terminal_print():
    print("hello world")
def move_up():
    print("moving up")
def move_down():
    print("moving down")
def move_right():
    print("moving right")
def move_left():
    print("moving left")
def move_upwards():
    print("moving upwards")
def move_downwards():
    print('moving downwards')

bottom = tk.Frame(master=main_window, bg='white')
bottom.pack_propagate(0)
bottom.pack(fill="both",expand=True)

up_click_btn = PhotoImage(file ='arrow_up.png')
down_click_btn = PhotoImage(file ='arrow_down.png')
right_click_btn = PhotoImage(file ='arrow_right.png')
left_click_btn = PhotoImage(file ='arrow_left.png')
upwards_click_btn = PhotoImage(file ='arrow_upwards.png')
downwards_click_btn = PhotoImage(file ='arrow_downwards.png')

up_button = tk.Button(master = bottom, image = up_click_btn, command = move_up, borderwidth = 0)
down_button = tk.Button(master = bottom, image = down_click_btn, command = move_down, borderwidth = 0)
right_button = tk.Button(master = bottom, image = right_click_btn, command = move_right, borderwidth = 0)
left_button = tk.Button(master = bottom, image = left_click_btn, command= move_left, borderwidth = 0)
upwards_button = tk.Button(master = bottom, image = upwards_click_btn, command= move_upwards, borderwidth = 0)
downwards_button = tk.Button(master = bottom, image = downwards_click_btn, command= move_downwards, borderwidth=0)
upload_ur_label = tk.Label(master = bottom, text="Upload URScript file that has to be sent to the robot", bg="white", font=("Helvetica", 9, "bold"))
upload_button = tk.Button(master=bottom, text='Upload', padx=15, pady=15, borderwidth=1, bg="cyan")
close = tk.Button(master=bottom, text='Quit', command=main_window.destroy, padx=15, pady=15, borderwidth=1, bg="red")
info_display_text = tk.Text(master=bottom, width=25, height=15)


upload_ur_label.grid(row=1, column=1, padx=15, pady=15)
upload_button.grid(row=2, column=1)
info_display_text.grid(row=3, column=1, rowspan=3, pady=10)
up_button.grid(row=3, column=3, padx=10, pady=10)
down_button.grid(row=5, column=3, padx=10, pady=10)
right_button.grid(row=4, column=4, padx=10, pady=10)
left_button.grid(row=4, column=2, padx=10, pady=10)
upwards_button.grid(row=3, column=6, padx=10)
downwards_button.grid(row=3, column=7, padx=10)


close.grid(row=6, column=8, pady=200, padx=270)

main_window.mainloop()