import socket
import struct
from time import sleep

class ReceiveClass:
	def __init__(self):
		self.setup()
		pass

	def setup(self):
		address = '192.168.127.128'
		port = 30001
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.connect((address, port))

	def get_attributes(self, data):
		if data:
			packlen =  (struct.unpack('!i', data[0:4]))[0]
			timestamp = (struct.unpack('!Q', data[10:18]))[0]
			packtype = (struct.unpack('!b', data[4:5]))[0]
			print ('packet length: ' + str(packlen))
			print ('timestamp: ' + str(timestamp))
			print ('packet type: ' + str(packtype))
			return packlen, timestamp, packtype 


	def get_cartesian(self, data):
		pass

	def socket_receive(self):
		data = self.sock.recv(4096)
		return data

	def main(self):
		while True:
			data = self.socket_receive()
			i = 0
			packlen, timestamp, packtype = self.get_attributes(data)
			if packtype == 16:
				while i+5 < packlen:
					msglen = (struct.unpack('!i', data[5+i:9+i]))[0] 
					msgtype = (struct.unpack('!b', data[9 + i:10 + i]))[0]
					if msgtype == 1:
						angle = [0]*6
						j = 0
						while j < 6:
								#cycle through joints and extract only current joint angle (double precision)  then print to screen
								#bytes 10 to 18 contain the j0 angle, each joint's data is 41 bytes long (so we skip j*41 each time)
							angle[j] = (struct.unpack('!d', data[10+i+(j*41):18+i+(j*41)]))[0]
							print('Joint ' + str(j) + ' angle : ' + str(angle[j]))
							j = j + 1
								 
						print('*******')
		
					elif msgtype == 4:
							#if message type is cartesian data, extract doubles for 6DOF pos of TCP and print to sc    reen
						x =  (struct.unpack('!d', data[10+i:18+i]))[0]
						y =  (struct.unpack('!d', data[18+i:26+i]))[0]
						z =  (struct.unpack('!d', data[26+i:34+i]))[0]
						rx =  (struct.unpack('!d', data[34+i:42+i]))[0]
						ry =  (struct.unpack('!d', data[42+i:50+i]))[0]
						rz =  (struct.unpack('!d', data[50+i:58+i]))[0]

						print( 'X:  ' + str(x))
						print('Y:  ' + str(y))
						print('Z:  ' + str(z))
						print('RX: ' + str(rx))
						print( 'RY: ' + str(ry))
						print( 'RZ: ' + str(rz))
						#increment i by the length of the message so move onto next message in packet
					i = msglen + i
					sleep(1)
if    __name__ == '__main__':
    import sys
    RC = ReceiveClass()
    RC.main()

