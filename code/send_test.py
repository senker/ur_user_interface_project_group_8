import socket
from time import sleep


class SendClass:
	def __init__(self):
		self.setup()

	def setup(self):
		address = '192.168.127.128'
		port = 30001
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((address, port))

	def send_bytes(self, message):
		byte_message = bytes(message, 'utf-8')
		self.sock.send(byte_message)

	def send_robot_true(self):
		digital_true = [
		"set_digital_out(1,True)" + "\n",
		"set_digital_out(2,True)" + "\n"
		]
		for item in digital_true:
			sleep(0.3)
			self.send_bytes(item)
			sleep(2)

	def send_robot_false(self):
		digital_false = [
		"set_digital_out(1,False)" + "\n",
		"set_digital_out(2,False)" + "\n"
		]
		for item in digital_false:
			sleep(0.3)
			self.send_bytes(item)

	def move_pattern1(self):
		self.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
		sleep(10)

	def move_pattern2(self):
		# Movej = move joints
		self.send_bytes("movej([-1.95, -1.58, 1.16, -1.15, -1.55, 1.18], a=1.0, v=0.1)" + "\n")
		sleep(10)

	def move_pattern3(self):
		pass
		sleep(10)

	def move_pattern4(self):
		pass
		sleep(10)

	def move_pattern5(self):
		pass
		sleep(10)

	def move_pattern6(self):
		pass
		sleep(10)

	def send_loop(self):

		self.send_robot_true()

		self.move_pattern1()

		self.move_pattern2()

		self.send_robot_false()

		data = self.sock.recv(1024)
		self.sock.close()
		if data:
			print ("Received")



if __name__ == '__main__':
    SC = SendClass()
    SC.send_loop()
