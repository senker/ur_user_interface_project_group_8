import tkinter as tk
from tkinter import *
from tkinter import ttk
from send import SendClass
from receive import ReceiveClass
from tkinter import filedialog as fd
from time import sleep

class GuiClass:
    def __init__(self, socket):
        self.sock = socket
        self.sender = SendClass(self.sock)
        self.receive = ReceiveClass(self.sock)
        self.gui_setup()
        self.component_setup()
        self.main_window.mainloop()


    def gui_setup(self):
        self.main_window = tk.Tk()
        self.main_window.title('UR Movement commands GUI')
        self.main_window.geometry("700x500+250+200")
        self.bottom = tk.Frame(master=self.main_window, bg='#282923')
        self.bottom.pack_propagate(0)
        self.bottom.pack(fill="both",expand=True)


    def move_up(self):
        self.sender.prep_robot()
        self.sender.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot straight\n")
        sleep(3)
        self.sender.false_robot()

    def move_down(self):
        self.sender.prep_robot()
        self.sender.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot back\n")
        sleep(3)
        self.sender.false_robot()

    def move_right(self):
        self.sender.prep_robot()
        self.sender.send_bytes("movej([-1.95, -1.58, 1.16, -1.15, -1.55, 1.18], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot right\n")
        sleep(3)
        self.sender.false_robot()

    def move_left(self):
        self.sender.prep_robot()
        self.sender.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot left\n")
        sleep(3)
        self.sender.false_robot()

    def move_upwards(self):
        self.sender.prep_robot()
        self.sender.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot upwards\n")
        sleep(3)
        self.sender.false_robot()

    def move_downwards(self):
        self.sender.send_bytes("movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n")
        self.info_display_text.insert("1.0","Moving the UR robot downwards\n")
        sleep(3)
        self.sender.false_robot()

    def quit_button(self):
        self.sender.socket_close()
        self.main_window.destroy

    def upload_file(self):
        print("upload le button")
        filetypes = (
            ('JSON files', '*.json'),
            ('text files', '*.txt'))
        filename = fd.askopenfile(filetypes=filetypes)
        for line in filename.readlines():
            print(line)
            self.sender.send_bytes(line)

    def display_information(self):
        #packlen, packtype = self.receive.receive_index()
        #self.info_display_text.insert("1.0", f"{packlen}{packtype}")
        try:
            self.info_display_text.insert("1.0", self.receive.receive_index())
        except TclError:
            pass
        
    def up_button_init(self):
        self.up_click_btn = PhotoImage(file ='images/arrow_up.png')
        self.up_button = tk.Button(master = self.bottom, image = self.up_click_btn, command = self.move_up, borderwidth = 1, bg="#282923")
        self.up_button.grid(row=3, column=3, padx=10, pady=10)
        
    def down_button_init(self):
        self.down_click_btn = PhotoImage(file ='images/arrow_down.png')
        self.down_button = tk.Button(master = self.bottom, image = self.down_click_btn, command = self.move_down, borderwidth = 1, bg="#282923")
        self.down_button.grid(row=5, column=3, padx=10, pady=10)
        
    def right_button_init(self):
        self.right_click_btn = PhotoImage(file ='images/arrow_right.png')
        self.right_button = tk.Button(master = self.bottom, image = self.right_click_btn, command = self.move_right, borderwidth = 1, bg="#282923")
        self.right_button.grid(row=4, column=4, padx=10, pady=10)
        
    def left_button_init(self):
        self.left_click_btn = PhotoImage(file ='images/arrow_left.png')
        self.left_button = tk.Button(master = self.bottom, image = self.left_click_btn, command= self.move_left, borderwidth = 1, bg="#282923")
        self.left_button.grid(row=4, column=2, padx=10, pady=10)
        
    def upwards_button_init(self):
        self.upwards_click_btn = PhotoImage(file ='images/arrow_upwards.png')
        self.upwards_button = tk.Button(master = self.bottom, image = self.upwards_click_btn, command= self.move_upwards, borderwidth = 1, bg="#282923")
        self.upwards_button.grid(row=3, column=6, padx=10)
    
    def downwards_button_init(self):
        self.downwards_click_btn = PhotoImage(file ='images/arrow_downwards.png')
        self.downwards_button = tk.Button(master = self.bottom, image = self.downwards_click_btn, command= self.move_downwards, borderwidth = 1, bg="#282923")
        self.downwards_button.grid(row=3, column=7, padx=10)
        
    def upload_ur_label_init(self):
        self.upload_ur_label = tk.Label(master = self.bottom, text="Upload URScript file that has to be sent to the robot", bg="#282923", fg="white", font=("Arial", 9, "bold"))
        self.upload_ur_label.grid(row=1, column=1, padx=15, pady=15)
        
    def upload_button_init(self):
        self.upload_button = tk.Button(master=self.bottom, text='Upload', padx=15, pady=15, borderwidth=3, command=self.upload_file, bg="#282923", fg="white", font=("Arial", 9, "bold"))
        self.upload_button.grid(row=2, column=1)

    def get_status_button_init(self):
        self.upload_button = tk.Button(master=self.bottom, text='Get status', padx=15, pady=15, borderwidth=3, command=self.display_information, bg="#282923", fg="white", font=("Arial", 9, "bold"))
        self.upload_button.grid(row=2, column=2)
        
        
    def close_init(self):
        self.close = tk.Button(master=self.bottom, text='Quit', command=self.quit_button, padx=15, pady=15, borderwidth=1, bg="#3b060e", fg="#b3a8a9")
        self.close.grid(row=5, column=6, pady=1, padx=1)

    def info_display_text_init(self):
        self.info_display_text = tk.Text(master=self.bottom, width=37, height=20, borderwidth=4, bg="#282923", fg="white")
        self.info_display_text.grid(row=3, column=1, rowspan=3, columnspan=1, pady=10, padx=5)

    def component_setup(self):
        self.up_button_init()
        self.down_button_init()
        self.right_button_init()
        self.left_button_init()
        self.upwards_button_init()
        self.downwards_button_init()
        self.upload_ur_label_init()
        self.upload_button_init()
        self.get_status_button_init()
        self.close_init()
        self.info_display_text_init()
