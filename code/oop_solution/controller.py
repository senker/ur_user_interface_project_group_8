import socket
from receive import ReceiveClass
from send import SendClass
from gui import GuiClass


class ControllerClass:
    def __init__(self):
        self.socket_setup()
        self.sender = SendClass(self.sock)
        self.gui = GuiClass(self.sock)
        self.receive = ReceiveClass(self.sock)

    def socket_setup(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        host = "192.168.127.128"
        #host = "192.168.142.128"
        port = 30001
        try:
            self.sock.connect((host, port))
        except socket.error:
            print('could not connect to socket')
            exit(0)

    def start_receive(self):
        self.receive.receive_loop()

    def loop(self):
        try:
            self.start_receive()
        except KeyboardInterrupt:
            print('Interrupted')
            self.sender.socket_close()
            exit(0)
