import socket
from struct import unpack
from time import sleep


class ReceiveClass:
	def __init__(self, socket):
		import sys
		self.sock = socket

	def get_attributes(self, data):
		if data:
			packlen = (unpack('!i', data[0:4]))[0]
			packtype = (unpack('!b', data[4:5]))[0]
			#print (f'packet type:{str(packtype)}')
			return packlen, packtype 

	def get_msg_info(self, data, i):
		if data:
			msglen = (unpack('!i', data[5+i:9+i]))[0] 
			msgtype = (unpack('!b', data[9+i:10+i]))[0]
			return msglen, msgtype


	def get_joint(self, data, angle, i, j):
		if data:
			angle[j] = (unpack('!d', data[10+i+(j*41):18+i+(j*41)]))[0]
			print(f'Joint{str(j)} angle:{angle[j]:.2f}')
			


	def get_cartesian(self, data, i):
		if data:
			print("-"*30)
			print('Cartesian data in subpackage:')
			x = (unpack('!d', data[10+i:18+i]))[0]
			y = (unpack('!d', data[18+i:26+i]))[0]
			z = (unpack('!d', data[26+i:34+i]))[0]
			rx = (unpack('!d', data[34+i:42+i]))[0]
			ry = (unpack('!d', data[42+i:50+i]))[0]
			rz = (unpack('!d', data[50+i:58+i]))[0]
			print(f'X: {x:.3f}')
			print(f'Y: {y:.3f}')
			print(f'Z: {z:.3f}')
			print(f'RX: {rx:.3f}')
			print(f'RY: {ry:.3f}')
			print(f'RZ: {rz:.3f}')
			print("-"*30)


	def socket_receive(self):
		data = self.sock.recv(4096)
		return data

	def receive_index(self):
		print('\nMonitoring started, Ctrl-C to exit monitoring and go back to GUI')
		while True:
			try:
				data = self.socket_receive()
				i = 0
				packlen, packtype = self.get_attributes(data)
				if packtype == 16:
					print(f'packet type: {str(packtype)} detected, indexing...')
					while i+5 < packlen:
						msglen, msgtype = self.get_msg_info(data, i)
						if msgtype == 1:
							angle = [0]*6
							j = 0
							print("-"*30)
							print('Joint data in subpackage:')
							while j < 6:
								self.get_joint(data, angle, i, j)
								j = j + 1
						elif msgtype == 4:
							self.get_cartesian(data, i)
						i = msglen + i
						sleep(2)
			except KeyboardInterrupt:
				print("Monitoring canceled, returning to GUI...\n")
				break






