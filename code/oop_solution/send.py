import socket
from time import sleep


class SendClass:
	def __init__(self, socket):
		self.sock = socket


	def send_bytes(self, message):
		byte_message = bytes(message, 'utf-8')
		self.sock.send(byte_message)

	def prep_robot(self):
		self.send_bytes("set_digital_out(1,True)" + "\n")
		sleep(0.3)
		self.send_bytes("set_digital_out(2,True)" + "\n")
		sleep(2)

	def false_robot(self):
		self.send_bytes("set_digital_out(1,False)" + "\n")
		sleep(0.3)
		self.send_bytes("set_digital_out(2,False)" + "\n")

	def socket_close(self):
		if self.sock:
			self.false_robot()
			self.sock.close()
