import socket
from time import sleep
import struct

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = "192.168.127.128"
port = 30001
sock.connect((host, port))

while True:
	data = sock.recv(4096)
	packet_length =  (struct.unpack('!i', data[0:4]))[0]
	packet_type = (struct.unpack('!b', data[4:5]))[0] 
	print(f'packet packet length received ={packet_length}')
	print(f'Package type received = {packet_type}')
	i = 0
	#if packet type is Robot State
	sleep(0.3)
	if packet_type == 16:
		print('received a robot state msg')
		#while i+5 < packet_length:
			#msglen = (struct.unpack('!i', data[5+i:9+i]))[0] 
			#msgtype = (struct.unpack('!b', data[9+i]))[0] 