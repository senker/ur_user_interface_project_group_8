import socket
from time import sleep
HOST = "192.168.137.2" # The remote host
PORT = 30001 # The same port as used by the server

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))


data0 = "set_digital_out(1,True)" + "\n"
byte_string0 = bytes(data0, 'utf-8')
s.send(byte_string0)
sleep(0.3)

data1 = "set_digital_out(2,True)" + "\n"
byte_string1 = bytes(data1, 'utf-8')
s.send(byte_string1)
sleep(2)

data2 = "movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n"
byte_string2 = bytes(data2, 'utf-8')
s.send(byte_string2)
sleep(10)

data3 = "movej([-1.95, -1.58, 1.16, -1.15, -1.55, 1.18], a=1.0, v=0.1)" + "\n"
byte_string3 = bytes(data3, 'utf-8')
s.send(byte_string3)
sleep(10)

data4 = "set_digital_out(1,False)" + "\n"
byte_string4 = bytes(data4, 'utf-8')
s.send(byte_string4)
sleep(0.3)

data5 = "set_digital_out(2,False)" + "\n"
byte_string5 = bytes(data5, 'utf-8')
s.send(byte_string5)


data = s.recv(1024)
s.close()
if data:
	print ("Received")