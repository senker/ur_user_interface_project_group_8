import socket
import struct

class ReceiveClass:

	def __init__(self):
		self.setup()

	def setup(self):
		pass
	
	def receive(self):
		self.data = self.sock.recv(1024)

	def get_attributes(self):
		if self.data:
			self.packet_time = struct.unpack("!Q", self.data[10:18])
			self.packet_length = struct.unpack("!i", self.data[0:4])
			self.packet_type  = struct.unpack("!b", self.data[4])

		else:
			pass

	def get_contents(self):
		if self.packet_type == 16:
			counter0 = self.create_counter()
			while counter0 + 5 < self.packet_length:
				self.message_type = struct.unpack("!b", self.data[9+counter0])
				if self.message_type == 1:
					counter1 = self.create_counter()
					while counter1 < 6:
						self.angle_list = []
						self.angle_list.append(struct.unpack("!d", self.data[10+counter0+(counter1*41):18+counter0+(counter1*41)]))
						counter1 += 1

				else:
					pass



	def create_counter(self):
		return 0


	def reset_start(self):
		self.counter += self.packet_length


		



if __name__ == '__main__':
    rc=ReceiveClass()