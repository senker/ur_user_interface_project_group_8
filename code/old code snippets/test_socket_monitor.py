import socket
from time import sleep
import struct

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = "192.168.17.128"
port = 30001
s.connect((host, port))

while True:
	data = s.recv(4096)
	#print(data)
	packet_length =  (struct.unpack('!i', data[0:4]))[0]
	packet_type = (struct.unpack('!b', data[4:5]))[0] 
	print(packet_length)
	print(packet_type)