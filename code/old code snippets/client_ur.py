import socket
from time import sleep

def socket_setup():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = "192.168.17.128"
	try:
		port = 30001
		s.connect((host, port))
	except:
		port = 30002
		s.connect((host, port))
	return s


def load_ur_script():
	filehand = open("ur_script.txt", "r")
	send_string = 'hello world'
	#filehand.read()
	return send_string


def send_command(s, send_string):
	send_bytes = str.encode(send_string)
	s.send(send_bytes)
	print("im sending")


s = socket_setup()
send_string = load_ur_script()

while True:
	send_command(s, send_string)
	sleep(3)
