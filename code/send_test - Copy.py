import socket
from time import sleep


class SendClass:
	def __init__(self):
		self.setup()

	def setup(self):
		address = '192.168.127.128'
		port = 30001
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((address, port))

	def send_bytes(self, message):
		byte_message = bytes(message, 'utf-8')
		self.sock.send(byte_message)

	def send_loop(self):
		message = "set_digital_out(1,True)" + "\n"
		self.send_bytes(message)
		sleep(0.3)

		message = "set_digital_out(2,True)" + "\n"
		self.send_bytes(message)
		sleep(2)

		message = "movej([0.00, 0.3, 0.4, 2.22, -2.22, 0.00], a=1.0, v=0.1)" + "\n"
		self.send_bytes(message)
		sleep(10)

		message = "movej([-1.95, -1.58, 1.16, -1.15, -1.55, 1.18], a=1.0, v=0.1)" + "\n"
		self.send_bytes(message)
		sleep(10)

		message = "set_digital_out(1,False)" + "\n"
		self.send_bytes(message)
		sleep(0.3)

		message = "set_digital_out(2,False)" + "\n"
		self.send_bytes(message)

		data = self.sock.recv(1024)
		self.sock.close()
		if data:
			print ("Received")



if __name__ == '__main__':
    SC = SendClass()
    SC.send_loop()
